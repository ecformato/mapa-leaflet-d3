const dataPreciosMadrid = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=1135447329&single=true&output=tsv';
const dataPreciosBarcelona = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=1934243624&single=true&output=tsv';
const dataComparativaPrecios = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=898139796&single=true&output=tsv';
const dataAnunciosMadrid = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=1294116346&single=true&output=tsv';
const dataAnunciosBarcelona = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=851873560&single=true&output=tsv';
const dataComparativaAnuncios = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=867598953&single=true&output=tsv';
const dataNombresBarrios = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTd7_oWpmsbfDOFTJpOlR1rqD21cGfqm78tCDP4kGaTahnx0YrrGgAy8QoerC1iC-XiYNbjbXHna03W/pub?gid=932699723&single=true&output=tsv';

let nombresBarrios = [], preciosMadrid = [], preciosBarcelona = [], comparativaPrecios = [], anunciosMadrid = [], anunciosBarcelona = [], comparativaAnuncios = [];
let arraySearch = [];

let urlParams = new URLSearchParams(window.location.search);
let typeParam1 = urlParams.get('datos');

let promesas = [d3.json('district_sections/barrios_barcelona.json'), d3.json('district_sections/barrios_madrid.json')];
let todasPromesas = Promise.all(promesas);
let barrioActive = 'lavapies-embajadores';

getData(dataNombresBarrios, 'nombres');
if(typeParam1 == 'precios'){
    getData(dataPreciosMadrid, 'preciosMadrid');
    getData(dataPreciosBarcelona, 'preciosBarcelona');
    getData(dataComparativaPrecios, 'comparativaPrecios');
    if(isMobile()){
        createSearch('madrid', 'precios');
    } else {
        createMap('precios');
    }    
} else if (typeParam1 == 'anuncios'){
    getData(dataAnunciosMadrid, 'anunciosMadrid');
    getData(dataAnunciosBarcelona, 'anunciosBarcelona');
    getData(dataComparativaAnuncios, 'comparativaAnuncios');
    if(isMobile()){
        createSearch('madrid', 'anuncios');
    } else {
        createMap('anuncios');
    }
}

/* Función para recoger los datos desde Google SpreadSheets */
function getData(data, type) {
    let http = new XMLHttpRequest();
    http.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = http.response;
            if(response == ""){
                console.error('No hay nada. Error en la llamada');
            } else {
                //Recogemos los datos necesarios
                if(type == 'preciosMadrid'){
                    preciosMadrid = csvToJson(response);
                } else if (type == 'preciosBarcelona'){
                    preciosBarcelona = csvToJson(response);
                } else if (type == 'comparativaPrecios'){
                    comparativaPrecios = csvToJson(response);
                } else if(type == 'anunciosMadrid'){
                    anunciosMadrid = csvToJson(response);
                } else if (type == 'anunciosBarcelona'){
                    anunciosBarcelona = csvToJson(response);
                } else if (type == 'comparativaAnuncios'){
                    comparativaAnuncios = csvToJson(response);
                } else {
                    nombresBarrios = csvToJson(response);
                }
            }   
        }
    }
    http.open('get', data, false);
    http.send();
}

function createMap(tipoDato){
    todasPromesas.then((data) => {
        //Aquí pintaríamos el mapa y jugaríamos con las variables indicadas en función del tipo de dato
        let conjuntoDatos = {type: 'FeatureCollection', features: []};

        data[0].features.map((item) => {
            conjuntoDatos.features.push(item)
        });
        data[1].features.map((item) => {
            conjuntoDatos.features.push(item)
        });

        let propiedadesConjunto = conjuntoDatos.features;

        if(tipoDato == 'precios'){
            comparativaPrecios.forEach((item) => {   
                propiedadesConjunto.filter((item2) => {if(item.Barrio == item2.properties.barrio_js) item2.diferencia_comparativa = +item.Diferencia.replace(',','.') })
            });
        } else {            
            comparativaAnuncios.forEach((item) => {                
                propiedadesConjunto.filter((item2) => {if(item.Barrio == item2.properties.barrio_js) item2.diferencia_comparativa = +item.Diferencia.replace(',','.') })
            });
        }

        let mymap = L.map('map').setView([40.475, -3.535], 10.5);

        mymap.touchZoom.disable();
        mymap.doubleClickZoom.disable();
        mymap.scrollWheelZoom.disable();
        mymap.boxZoom.disable();
        mymap.keyboard.disable();

        let CartoDB_DarkMatter = L.tileLayer('https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            subdomains: 'a',
            maxZoom: 15,
            minZoom: 9
        }).addTo(mymap);

        L.svg({clickable:true}).addTo(mymap);

        let svg = d3.select("#map").select("svg")
            .attr("pointer-events", "auto");
            
        let g = svg.select("g")

        function projectPoint(x, y) {
            let point = mymap.latLngToLayerPoint(new L.LatLng(y, x));
            this.stream.point(point.x, point.y);
        }

        let transform = d3.geoTransform({point: projectPoint}),
            path = d3.geoPath().projection(transform);

        lines = g.selectAll("path .madrid")
            .data(propiedadesConjunto)
            .enter()
            .append("path")
            .attr('class', (d) => {if(d.diferencia_comparativa || d.diferencia_comparativa == 0){return 'active'}})
            .attr("d", path)
            .attr("fill", (d) => {
                if(d.diferencia_comparativa > 0){
                    return '#A7C9BA';
                } else if (d.diferencia_comparativa == 0){
                    return 'lightgrey';
                } else if (d.diferencia_comparativa < 0){
                    return '#FF6666';
                } else {
                    return 'white';
                }
            })
            .attr("fill-opacity", (d) => {if(d.properties.barrio_js == barrioActive){return '1'}else{return '0.7'}})
            .attr("stroke", "#3b3b3b")
            .attr("stroke-width",'0.7px')
            .on('click', (d) => {
                if(d.diferencia_comparativa || d.diferencia_comparativa == 0){
                    if(d.properties.name){
                        createName(d.properties.barrio_js);
                        createChart(d.properties.barrio_js, typeParam1, 'madrid');                        
                    } else {
                        createName(d.properties.barrio_js);
                        createChart(d.properties.barrio_js, typeParam1, 'barcelona');
                    }
                    createData(d.properties.barrio_js, typeParam1);
                }
                barrioActive = d.properties.barrio_js;
                reset();                
            });

        mymap.on('viewreset', reset);
        mymap.on('zoom', reset);

        function reset(){
            lines
                .attr("d", path)
                .attr("fill", (d) => {
                    if(d.diferencia_comparativa > 0){
                        return '#A7C9BA';
                    } else if (d.diferencia_comparativa == 0){
                        return 'lightgrey';
                    } else if (d.diferencia_comparativa < 0){
                        return '#FF6666';
                    } else {
                        return 'white';
                    }
                })
                .attr("fill-opacity", (d) => {if(d.properties.barrio_js == barrioActive){return '1'}else{return '0.7'}})
                .attr("stroke", "#3b3b3b")
                .attr("stroke-width",'0.7px');
        }

        function updateMap(ciudad){
            ciudad == 'madrid' ? mymap.setView(new L.LatLng(40.475, -3.55), 11) : mymap.setView(new L.LatLng(41.39, 2.239), 12);
            if(ciudad == 'madrid'){
                barrioActive = 'lavapies-embajadores';
                createName(barrioActive);
                createChart(barrioActive, typeParam1, 'madrid');       
                createData(barrioActive, typeParam1);
                reset();
            } else {
                barrioActive = 'la-barceloneta';
                createName(barrioActive);
                createChart(barrioActive, typeParam1, 'barcelona');       
                createData(barrioActive, typeParam1);
                reset();
            }
        }   

        // //////////

        document.getElementsByTagName('button')[0].addEventListener('click', (e) => {
            if (!e.target.classList.contains("btn--active")) {
                document.getElementsByClassName('btn--active')[0].classList.remove('btn--active');
                e.target.classList.add("btn--active");
                cambioCiudad(e.target.value);
            }
        });
        document.getElementsByTagName('button')[1].addEventListener('click', (e) => {
            if (!e.target.classList.contains("btn--active")) {
                document.getElementsByClassName('btn--active')[0].classList.remove('btn--active');
                e.target.classList.add("btn--active");
                cambioCiudad(e.target.value);
            }
        });            

        function cambioCiudad(valor){
            valor == 'madrid' ? updateMap('madrid') : updateMap('barcelona');
        }

        // ///////        
        createName(barrioActive);
        createChart(barrioActive, typeParam1, 'madrid');       
        createData(barrioActive, typeParam1);
    });    
}

function createSearch(ciudad) {
    //Borramos todos los hijos del buscador para nutrir de nuevos -> Sucede al cambiar de ciudad
    let datalist = document.getElementById('json-datalist');
    while(datalist.firstChild){
        datalist.removeChild(datalist.firstChild);
    }

    let newArray = nombresBarrios.filter((item) => {
        if(item.Ciudad.trim().toLowerCase() == ciudad){
            return item;
        }
    });

    //Volvemos a tener cero elementos en el array
    arraySearch = [];
    
    for(let i = 0; i < newArray.length; i++){
        //Creamos los elementos del array comparativo
        arraySearch.push({nombre_real: newArray[i].Barrio_Mostrar, nombre_desarrollo: newArray[i].Barrio_Desarrollo, ciudad: newArray[i].Ciudad.toLowerCase()})
        //Creamos los elementos en HTML
        let newOption = document.createElement('option');
        newOption.value = newArray[i].Barrio_Mostrar;
        datalist.appendChild(newOption);
    }

    //Asociamos los botones de las ciudades a eventos del buscador
    document.getElementsByTagName('button')[0].addEventListener('click', (e) => {
        if (!e.target.classList.contains("btn--active")) {
            document.getElementsByClassName('btn--active')[0].classList.remove('btn--active');
            e.target.classList.add("btn--active");
            cambioCiudadBuscador(e.target.value);
        }
    });
    document.getElementsByTagName('button')[1].addEventListener('click', (e) => {
        if (!e.target.classList.contains("btn--active")) {
            document.getElementsByClassName('btn--active')[0].classList.remove('btn--active');
            e.target.classList.add("btn--active");
            cambioCiudadBuscador(e.target.value);
        }
    });

    function cambioCiudadBuscador(ciudad){
        document.getElementById('nombreBarrio').value = '';
        ciudad == 'madrid' ? createSearch('madrid') : createSearch('barcelona');
    }

    // //////Buscador por defecto
    if(ciudad == 'madrid'){
        setValueSearch('lavapies-embajadores', 'madrid');
    } else {
        setValueSearch('la-barceloneta', 'barcelona');
    }    
}

document.getElementById('nombreBarrio').addEventListener('change', (e) => {
    if(e.target.value != ''){
        arraySearch.filter((item) => {
            if(item.nombre_real == e.target.value){
                setValueSearch(item.nombre_desarrollo, item.ciudad);
            }
        });
        e.target.blur();
    }    
})

function setValueSearch(barrio, ciudad){
    createName(barrio);
    createChart(barrio, typeParam1, `${ciudad.toLowerCase().trim()}`);       
    createData(barrio, typeParam1);
}

//Funciones CARD
function createName(barrio){
    let mostrarNombre = nombresBarrios.filter((item) => {return item.Barrio_Desarrollo == barrio && item});
    let nombre = mostrarNombre[0].Barrio_Mostrar;
    document.getElementById('sectionName').textContent = `${nombre.toUpperCase()}`;
}

function createData(barrio, tipoDato){
    let newData = [];
    if(tipoDato == 'anuncios'){
        newData = comparativaAnuncios.filter((item) => {return item.Barrio == barrio && item});   
    } else {
        newData = comparativaPrecios.filter((item) => {return item.Barrio == barrio && item});      
    }
    document.getElementById('dataDiferencia').textContent = `${newData[0].Diferencia}%`;
}

//Tooltip para gráfico
let tooltipDiv = d3.select('body').append('div')
.style('background', '#a7c9c7')
.style('top', '0px')
.style('left', '0px')
.style('width', '90px')
.style('border-radius', '2px')
.style('padding', '5px')
.style('position', 'absolute')
.style("z-index", "2000")
.style("display", "none")
.style('font-size', '0.9rem')
.style('color', '#000')
.style('text-align','center');

function createChart(barrio, tipoDato, ciudad) { 
    let newData = [];

    let grafico = document.getElementById('chart');
    while(grafico.firstChild){
        grafico.removeChild(grafico.firstChild);
    }

    if(tipoDato == 'anuncios'){
        if(ciudad == 'madrid'){
            anunciosMadrid.filter((item) => {
                newData.push({fecha: formatTime(item.Fecha), barrio: +item[`${barrio}`].replace(',','.')})
            })
        } else {
            anunciosBarcelona.filter((item) => {
                newData.push({fecha: formatTime(item.Fecha), barrio: +item[`${barrio}`].replace(',','.')})
            })
        }
    } else {
        if(ciudad == 'madrid'){
            preciosMadrid.filter((item) => {
                newData.push({fecha: formatTime(item.Fecha), barrio: +item[`${barrio}`].replace(',','.')})
            })
        } else {
            preciosBarcelona.filter((item) => {
                newData.push({fecha: formatTime(item.Fecha), barrio: +item[`${barrio}`].replace(',','.')})
            })
        }
    }
    
    let margin = {top: 20, right: 20, bottom: 30, left: 45}
    , width = document.getElementById('chart').clientWidth - margin.left - margin.right 
    , height = document.getElementById('chart').clientHeight - margin.top - margin.bottom;

    let chart = d3.select("#chart").append("svg")
        .attr('id','sectionChart')
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = d3.scaleTime()
        .range([0,width])
        .domain([new Date(2019,02,26), new Date(2020,05,15)]);

    let xAxis = svg => svg
        .call(d3.axisBottom(x).ticks(0))
        .call(g => g.selectAll('path').attr('stroke','#a7c9c7'));

    chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    //Segundo eje X
    let x_v2 = d3.scaleBand()
        .range([0,width])
        .domain([1,2]);
    
    let xAxis_v2 = svg => svg
        .call(d3.axisBottom().tickFormat((d) => {return d == 1 ? '03/19' : '06/20' }).scale(x_v2).ticks(1))
        .call(g => g.selectAll('.tick').attr('transform', (d) => {return d == 1 ? `translate(0,0)` : `translate(${document.getElementById('sectionChart').getBoundingClientRect().width - 62.5},0)`}))
        .call(g => g.selectAll('.tick text').attr('y',11.5).style('font-family','Roboto').style('font-weight','200').style('font-size','13px').style('fill','#818181').attr('text-anchor', (d) => { return d == 1 ? 'start' : 'end'}))
        .call(g => g.selectAll('path').remove());
    
    chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis_v2);

    //Eje Y
    let y = d3.scaleLinear()
        .range([height, 0])
        .domain(d3.extent(newData, (d) => { return d.barrio }));

    let yAxis = svg => svg
        .call(d3.axisLeft(y).ticks(3).tickFormat((d) => {return tipoDato == 'precios' ? d + '€' : d}))
        .call(g => g.selectAll('.tick text').style('font-family','Roboto').style('font-weight','200').style('font-size','13px').style('fill','#818181'))
        .call(g => g.selectAll('.tick text'))
        .call(g => g.selectAll('line').remove())
        .call(g => g.select('path').style('stroke','#a7c9c7'));
    
    chart.append("g")
        .call(yAxis);

    //Línea COVID
    chart.append("svg:line")
        .attr("stroke-width", "1px")
        .attr("stroke", "#ff6666")
        .attr("x1", x(new Date(2020,02,14)))
        .attr("y1", height)
        .attr("x2", x(new Date(2020,02,14)))
        .attr("y2", 0);

    //Lógica línea
    let line = d3.line()
        .curve(d3.curveCatmullRom)
        .x(function(d) { return x(d.fecha); })
        .y(function(d) { return y(d.barrio); });

    //Dibujo línea
    chart.append("path")
        .data([newData])
        .attr("fill", "none")
        .attr("stroke", "#a7c9c7")
        .attr("stroke-width","1px")
        .attr("d", (d) => { return line(d) });

    //Dibujo círculos
    chart.selectAll("circle")
        .data(newData)
        .enter()
        .append("circle")
        .attr("r", 3)
        .attr("cx", function(d) { return x(d.fecha) })
        .attr("cy", function(d) { return y(d.barrio) })
        .style("fill", '#a7c9c7')
        .style('opacity','0')
        .on('mouseover', (d, i, f) => {
            let circulo = f[i];
            circulo.style.opacity = '1';

            let bodyWidth = parseInt(d3.select(`#chart`).style('width'));
            let mapHeight = parseInt(d3.select(`#chart`).style('height'));

            let rigthSide = bodyWidth / 2 > d3.event.pageX ? false : true;
            let downSide = mapHeight / 2 > d3.event.pageY ? false : true;

            tooltipDiv.transition()     
                .duration(200)
                .style('display', 'block')   
            tooltipDiv.html(`<span style="font-weight: 900;">${getNumberWithCommas(d.barrio)}</span><span>${tipoDato == 'anuncios' ? ' anuncios' : ' €/m2'}</span>`)
                .style("left", (downSide && rigthSide ? d3.event.pageX - 65 : downSide && !rigthSide ? d3.event.pageX + 15 : !downSide && rigthSide ? d3.event.pageX - 60 : d3.event.pageX - 30) + "px")     
                .style("top", (downSide && rigthSide ? d3.event.pageY - 40 : downSide && !rigthSide ? d3.event.pageY - 40 : !downSide && rigthSide ? d3.event.pageY - 27.5 : d3.event.pageY - 32.5) + "px");
        })
        .on('mouseout', (d, i, f) => {
            let circulo = f[i];
            circulo.style.opacity = '0';
        });
}

function getOutTooltip(){
    tooltipDiv.style('display','none');
}

document.getElementById('chart').addEventListener('mouseleave', () => {
    getOutTooltip();
});

function csvToJson(csv){
    let lines=csv.split("\n");    
    let result = [];

    let headers = lines[0].split("\t");
    let inicio = 1;

    for(i = 0; i < headers.length; i++){
        headers[i] = headers[i].trim();
    }

    for(let i=inicio;i<lines.length;i++){
        let obj = {};
        let currentline=lines[i].split("\t");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){
                obj[headers[j]] = currentline[j];
            }            
        }
        result.push(obj);
    }    
    return result;
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

d3.timeFormatDefaultLocale({
    "decimal": ",",
    "thousands": ".",
    "grouping": [3],
    "currency": ["€", ""],
    "dateTime": "%a %b %e %X %Y",
    "date": "%d/%m/%Y",
    "time": "%H:%M:%S",
    "periods": ["AM", "PM"],
    "days": ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    "shortDays": ["Dom", "Lun", "Mar", "Mi", "Jue", "Vie", "Sab"],
    "months": ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
    "shortMonths": ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
});

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function isMobile() {
    return window.innerWidth < 767
}